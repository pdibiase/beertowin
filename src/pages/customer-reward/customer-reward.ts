import {Component} from "@angular/core";
import {Loading, NavController, NavParams} from "ionic-angular";
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-customer-reward',
  templateUrl: 'customer-reward.html'
})
export class CustomerRewardPage {

  private id_token;
  public reward: any;
  private loader: Loading;


  constructor(
    params: NavParams,
    public nav: NavController,
    public http: HttpClient,
    public local: Storage,
    public avatarService : AvatarService
  ) {

    this.reward = params.get('reward');

  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }

}
