import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";
import 'rxjs/add/operator/map';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {authProvider} from "../../providers/authProvider";
import {ValidationService} from "../../app/validation.service";
import {Facebook} from '@ionic-native/facebook';
import {TranslateService} from "@ngx-translate/core";
import {AvatarService} from "../../services/avatar-service";
import {Storage} from '@ionic/storage';


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-my-profile-update-password',
  templateUrl: 'my-profile-update-password.html'
})
export class MyProfileUpdatePasswordPage {


  private id_token;
  private loader: Loading;
  private error : any;
  form : FormGroup;


  constructor(
    public formBuilder: FormBuilder,
    public nav: NavController,
    public auth:authProvider,
    public fb: Facebook,
    private translate: TranslateService,
    public avatarService : AvatarService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public local: Storage
  ) {

    this.form = this.formBuilder.group({
      password: ['', [Validators.required, ValidationService.passwordValidator]],
      old_password: ['', [Validators.required, ValidationService.passwordValidator]],
      password_confirmation: ['', [Validators.required, ValidationService.passwordCompareValidator]]
    });


  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      console.log(id_token);
      this.id_token = id_token;

    }));

  }


  // sign up
  doUpdatePassword(credentials) {

    if( this.form.valid ){

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this.auth.updatePassword(credentials, this.id_token).subscribe(
        data => this.updatePasswordSuccess(data),
        err => this.updatePasswordError(err),
      );

    }

  }


  updatePasswordSuccess(data) {

    this.loader.dismissAll();
    this.error = null;


    let toast = this.toastCtrl.create({
      message: this.translate.instant('update-password-success'),
      duration: 3000,
      position: 'top'
    });
    toast.present();


    this.nav.setRoot( LoginPage );

  }


  updatePasswordError(err){

    this.loader.dismissAll();

    let toast = this.toastCtrl.create({
      message: this.translate.instant('error-update-password'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }


  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }


  getAvatarImage() {

    return this.avatarService.getItemImage(0);

  }

}
