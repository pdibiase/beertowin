import {Component, ViewChild} from "@angular/core";
import {Content, NavController, NavParams} from "ionic-angular";

@Component({
  selector: 'page-retailer-gallery',
  templateUrl: 'retailer-gallery.html'
})
export class RetailerGalleryPage {
  @ViewChild(Content) content: Content;


  public galleryArr;
  public showSlide: number = 0;


  constructor(
    public nav: NavController,
    public navParams: NavParams,
  ) {

    this.galleryArr = navParams.get('galleryArr');
    this.showSlide = navParams.get('showSlide');

  }


  ionViewDidLoad() {



  }

}
