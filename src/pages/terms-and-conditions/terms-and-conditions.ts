import {Component} from "@angular/core";
import {NavController, MenuController, ViewController, ToastController} from "ionic-angular";
import {AvatarService} from "../../services/avatar-service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {ValidationService} from "../../app/validation.service";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'terms-and-conditions',
  templateUrl: 'terms-and-conditions.html'
})
export class TermsAndConditionsPage {

  private userCommunicateAgree : string = this.translate.instant('not-agree');
  private userProfilingAgree : string = this.translate.instant('not-agree');
  private userMarketingAgree : string = this.translate.instant('not-agree');
  form : FormGroup;


  constructor(
    private toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public nav: NavController,
    public menuCtrl: MenuController,
    public avatarService : AvatarService,
    private translate: TranslateService,
  ) {

    this.menuCtrl.swipeEnable(false);


    this.form = this.formBuilder.group({
      user_privacy_agree: [false, [ValidationService.checkControl]],
      user_adult: [false, [ValidationService.checkControl]],
      user_communicate_data_agree: [false],
      user_profiling_agree: [false],
      user_marketing_agree: [false]
    });

  }



  acceptTerms(credentials) {

    if( this.form.value['user_privacy_agree'] && this.form.value['user_adult'] ){

      this.viewCtrl.dismiss(
        {
          "user_privacy_agree"          : this.form.value['user_privacy_agree'],
          "user_adult"                  : this.form.value['user_adult'],
          "user_communicate_data_agree" : this.form.value['user_communicate_data_agree'],
          "user_profiling_agree"        : this.form.value['user_profiling_agree'],
          "user_marketing_agree"        : this.form.value['user_marketing_agree']
      });

    } else {

      let toast = this.toastCtrl.create({
        message: this.translate.instant('privacy-set-all'),
        duration: 3000,
        position: 'top'
      });
      toast.present();

    }

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(0);

  }


  updateCommunicateLabel() {

    this.userCommunicateAgree = (this.userCommunicateAgree == this.translate.instant('agree'))?
      this.translate.instant('not-agree'):
      this.translate.instant('agree');

  }


  updateProfilingLabel() {

    this.userProfilingAgree = (this.userProfilingAgree == this.translate.instant('agree'))?
      this.translate.instant('not-agree'):
      this.translate.instant('agree');

  }


  updateMarketingLabel() {

    this.userMarketingAgree = (this.userMarketingAgree == this.translate.instant('agree'))?
      this.translate.instant('not-agree'):
      this.translate.instant('agree');

  }

}
