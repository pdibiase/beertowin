import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";
import 'rxjs/add/operator/map';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {authProvider} from "../../providers/authProvider";
import {ValidationService} from "../../app/validation.service";
import {TranslateService} from "@ngx-translate/core";
import {AvatarService} from "../../services/avatar-service";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-lost-password',
  templateUrl: 'lost-password.html'
})
export class LostPasswordPage {


  private loader: Loading;
  private error : any;
  form : FormGroup;


  constructor(
    public formBuilder: FormBuilder,
    public nav: NavController,
    public auth:authProvider,
    private translate: TranslateService,
    public avatarService : AvatarService,
    private loading: LoadingController,
    private toastCtrl: ToastController
  ) {

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]]
    });

  }



  doLostPassword(credentials) {

    if( this.form.valid ){

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this.auth.lostPassword(credentials).subscribe(
        data => this.lostPasswordSuccess(data),
        err => this.lostPasswordError(err),
      );

    }

  }


  lostPasswordSuccess(data) {

    this.loader.dismissAll();
    this.error = null;


    let toast = this.toastCtrl.create({
      message: this.translate.instant('lost-password-success'),
      duration: 3000,
      position: 'top'
    });
    toast.present();


    this.nav.setRoot( LoginPage );

  }


  lostPasswordError(err){

    this.loader.dismissAll();

    let toast = this.toastCtrl.create({
      message: this.translate.instant('error-lost-password'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }

  getAvatarImage() {

    return this.avatarService.getItemImage(0);

  }

}
