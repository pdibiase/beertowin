import {Component} from "@angular/core";
import {Loading, LoadingController, NavParams, Platform, ViewController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {badgeProvider} from "../../providers/badgeProvider";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {ENV} from "../../config/environment.prod";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {TranslateService} from "@ngx-translate/core";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-notification-modal',
  templateUrl: 'notification-modal.html'
})
export class NotificationModalPage {

  private id_token;
  private avatar_code;
  public notification: any;
  private loader: Loading;



  constructor(
    params: NavParams,
    public viewCtrl: ViewController,
    public http: HttpClient,
    public local: Storage,
    public badgeProvider : badgeProvider,
    public avatarService : AvatarService,
    public platform: Platform,
    private iab: InAppBrowser,
    private translate: TranslateService,
    private loading: LoadingController,
  ) {

    this.notification = params.get('notification');

  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }


  closeNotification(notification){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let url=  ENV.API_URL + "setNotificationRead";
    return this
      .http.post(
        url,
        {
          notification_id : notification.notification_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        this.loader.dismissAll();

        let res = JSON.parse(JSON.stringify(response));
        if( res && res.code == 200 ) {

          this.badgeProvider.decreaseBadges(1);

        }


        this.viewCtrl.dismiss();

      })
      .catch(this.handleError);

  }


  handleError() {

    this.loader.dismissAll();

  }



  openLink(link){

    if (this.platform.is('android') || this.platform.is('ios')) {

      this.iab.create(
        link,
        '_system',
        "location=yes"
      );

    } else {

      window.open(link);

    }

  }


}
