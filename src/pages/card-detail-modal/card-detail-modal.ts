import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {HttpClient} from "@angular/common/http";
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {AvatarService} from "../../services/avatar-service";
import {MainTabsPage} from "../main-tabs/main-tabs";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-card-detail-modal',
  templateUrl: 'card-detail-modal.html'
})
export class CardDetailModalPage {


  private id_token;
  private avatar_code;
  private loader: Loading;
  public card;
  public text: string = "";



  constructor(
    public http: HttpClient,
    public nav: NavController,
    public local: Storage,
    public navParams: NavParams,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public avatarService : AvatarService
  ) {

    this.card = navParams.get('card');

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  getAward(){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    let url=  ENV.API_URL + "requestCampaignAward";
    return this
      .http.post(
        url,
        {
          card_id: this.card.card_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        this.loader.dismissAll();


        let toast = this.toastCtrl.create({
          message: this.translate.instant('award-required'),
          duration: 3000,
          position: 'top'
        });
        toast.present();


        this.nav.setRoot( MainTabsPage );

      })
      .catch(error =>

        this.handleError(error)

      );

  }



  handleError(error) {

    this.loader.dismissAll();

    if( error && error.error && error.error.message ) {


      //SHOW ERROR MESSAGE
      error.error.message.forEach( item => {

        let toast = this.toastCtrl.create({
          message: item,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });

    }

  }

}
