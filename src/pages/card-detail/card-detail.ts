import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {HttpClient} from "@angular/common/http";
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {AvatarService} from "../../services/avatar-service";
import {MainTabsPage} from "../main-tabs/main-tabs";
import {SocialSharing} from "@ionic-native/social-sharing";
import {CardDetailModalPage} from "../card-detail-modal/card-detail-modal";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-card-detail',
  templateUrl: 'card-detail.html'
})
export class CardDetailPage {


  private id_token;
  private avatar_code;
  private requireButtonClass : string = "button-off";
  public lastItemImage = "assets/img/card/card_off.png";
  public cardItems: Array<Array<Array<boolean>>>;
  private loader: Loading;
  public card;
  public text: string = "";



  constructor(
    public http: HttpClient,
    public nav: NavController,
    public local: Storage,
    public navParams: NavParams,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public avatarService : AvatarService,
    private socialSharing: SocialSharing
  ) {

    this.card = navParams.get('card');
    this.printCardItems();
    this.setButtonBackground();
    this.setTextAward();

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  printCardItems(){

    if( this.card ){

      let memberPoints = parseInt( this.card.member_points );
      let totalPoints = parseInt( this.card.campaign_points );
      let currentPoints = totalPoints - memberPoints;


      //SET NUMBER OF ITEMS TO BE ACTIVATE
      if( currentPoints > 10 ){

        currentPoints = currentPoints % 10;

      }
      currentPoints = 10 - currentPoints;


      this.cardItems = [];
      let currentPos = 0;
      for ( let i=0; i<1; i++ ){

        this.cardItems[i] = [];
        for ( let j=0; j<totalPoints / 5; j++ ){

          this.cardItems[i][j] = [];
          for( let k=0; k< 5; k++){

            //if( currentPos < 9 ) {

              this.cardItems[i][j][k] = (currentPoints > currentPos);

            //}

            currentPos++;

          }

        }

      }


      //SET NUMBER OF ITEMS TO BE ACTIVATE
      /*if( currentPoints > 10 ){

        currentPoints = currentPoints % 10;

      }
      currentPoints = 10 - currentPoints;



      //PRINT FIRST ITEMS
      this.cardItems = [];
      let currentPos = 0;
      for ( let i=0; i<1; i++ ){

        this.cardItems[i] = [];
        for ( let j=0; j<4; j++ ){

          this.cardItems[i][j] = [];
          for( let k=0; k<3; k++){

            if( currentPos < 9 ) {

              this.cardItems[i][j][k] = (currentPoints > currentPos);

            }

            currentPos++;

          }

        }

      }


      //SET LAST ITEM
      this.lastItemImage = ( currentPoints >= 10 )?
        "assets/img/card/card_on.png" :
        "assets/img/card/card_off.png";*/


    }

  }


  setButtonBackground() {

    let memberPoints = parseInt( this.card.member_points );
    let totalPoints = parseInt( this.card.campaign_points );

    this.requireButtonClass = (memberPoints >= totalPoints)? "button-on" : "button-off";

  }



  setTextAward() {

    let memberPoints = parseInt( this.card.member_points );
    let totalPoints = parseInt( this.card.campaign_points );
    let awardRemain = ((totalPoints - memberPoints) > 0)? (totalPoints - memberPoints) : 0;

    if( awardRemain > 0) {

      this.text = this.translate.instant(
        'card-award-text',
        {
          points: memberPoints,
          totalPoints: totalPoints
        }
      );

    } else {

      this.text = this.translate.instant('card-award-text-1');

    }

  }


  getAward(){

    let memberPoints = parseInt( this.card.member_points );
    let totalPoints = parseInt( this.card.campaign_points );

    if( memberPoints >=  totalPoints){

      this.nav.push(CardDetailModalPage, { card: this.card });

    }

  }



  handleError(error) {

    this.loader.dismissAll();

    if( error && error.error && error.error.message ) {


      //SHOW ERROR MESSAGE
      error.error.message.forEach( item => {

        let toast = this.toastCtrl.create({
          message: item,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });

    }

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }


  getLastItemImage() {

    return this.lastItemImage;

  }


  openSocialSharing( ) {

    let memberPoints = parseInt( this.card.member_points );
    let totalPoints = parseInt( this.card.campaign_points );
    let awardRemain = ((totalPoints - memberPoints) > 0)? (totalPoints - memberPoints) : 0;


    this.local.get('user_name').then((user_name => {

      let name = this.translate.instant(
        'card-award-text-share',
        {
          user: user_name,
          retailer: this.card.user_nicename,
          awardRemain: awardRemain
        });


      this.socialSharing.share(name, name, "", "")
        .then(() => {

          let toast = this.toastCtrl.create({
            message: this.translate.instant('share-success'),
            duration: 3000,
            position: 'top'
          });
          toast.present();

        }).catch(() => {

        let toast = this.toastCtrl.create({
          message: this.translate.instant('share-error'),
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });


    }));

  }

}
