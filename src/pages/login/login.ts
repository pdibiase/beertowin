import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, ToastController} from "ionic-angular";
import {RegisterPage} from "../register/register";
import {MainTabsPage} from "../main-tabs/main-tabs";
import {FormBuilder, FormGroup} from "@angular/forms";
import {authProvider} from "../../providers/authProvider";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {TranslateService} from "@ngx-translate/core";
import {LostPasswordPage} from "../lost-password/lost-password";
import {ENV} from "../../config/environment.prod";
import {HomeRetailerPage} from "../home-retailer/home-retailer";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  private loader: Loading;
  form : FormGroup;


  constructor(
    public formBuilder: FormBuilder,
    public nav: NavController,
    public auth:authProvider,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    public avatarService : AvatarService,
    private toastCtrl: ToastController
  ) {

    this.form = this.formBuilder.group({
      email: [''],
      password: ['']
    });

  }



  getAvatarImage() {

    return this.avatarService.getItemImage(0);

  }

  // login
  doLogin( credentials ) {

    if( credentials.email && credentials.password ) {

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this.auth.login(credentials).subscribe(
        data => this.loginSuccess(data, credentials),
        err => this.authError(err)
      );

    }

  }



  authError(err){

    this.loader.dismissAll();

    let toast = this.toastCtrl.create({
      message: this.translate.instant('error-login'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }



  loginSuccess(data, credentials) {

    if(data.success){

      this.loader.dismissAll();

      localStorage.setItem('id_token', data.token);

      this.local.set('user_type', data.user_type);
      this.local.set('user_name', data.user_name);
      this.local.set('user_surname', data.user_surname);
      this.local.set('user_image', data.user_image);

      this.local.set('id_token', data.token);
      this.local.get('id_token').then((id_token => {

        if( data.user_type == ENV.TYPE_RETAILER ) {

          this.nav.setRoot(HomeRetailerPage);

        } else {

          this.nav.setRoot(MainTabsPage);

        }

      }));

    }

  }


  signUp() {

    this.nav.setRoot(RegisterPage);

  }


  goToLostPassword() {

    this.nav.push(LostPasswordPage);

  }


}
