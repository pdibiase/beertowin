import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {CardsModels} from "../../services/cards";
import {CardDetailPage} from "../card-detail/card-detail";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html'
})
export class CardsPage {


  private id_token;
  private loader: Loading;
  cards: CardsModels = new CardsModels();
  campaignTitle : boolean = false;



  constructor(
    public http: HttpClient,
    public nav: NavController,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    public navParams: NavParams,
    private toastCtrl: ToastController
  ) {

    if( navParams.get('cards') ){

      this.cards = navParams.get('cards');
      this.campaignTitle = true;


    }

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;




      if( !this.cards.cardModel ) {

        this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
        this.loader.present();

        this
          .getActiveCards()
          .then(
            data => {

              this.loader.dismissAll();
              this.cards.cardModel = data;

            }
          );

      }


    }));

  }


  getActiveCards(): Promise<any> {

    let url=  ENV.API_URL + "getCustomerCards";
    return this
      .http.post(
        url,
        null,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as CardsModels[] )
      .catch(this.handleError);

  }



  handleError() {

    if( this.loader ) {

      this.loader.dismissAll();

    }

  }


  openDetails (card) {

    if( card && card.campaignawards && card.campaignawards.length ){

      this.nav.push(CardDetailPage, { card: card });

    } else {

      let toast = this.toastCtrl.create({
        message: this.translate.instant('error-open-card'),
        duration: 3000,
        position: 'top'
      });
      toast.present();

    }


  }


}
