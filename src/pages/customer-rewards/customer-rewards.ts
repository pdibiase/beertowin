import {Component} from "@angular/core";
import {App, Loading, LoadingController, NavController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {RewardsModels} from "../../services/rewards";
import {CustomerRewardPage} from "../customer-reward/customer-reward";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */

@Component({
  selector: 'page-customer-rewards',
  templateUrl: 'customer-rewards.html'
})
export class CustomerRewardsPage {


  private id_token;
  private loader: Loading;
  rewards: RewardsModels = new RewardsModels();



  constructor(
    public app: App,
    public http: HttpClient,
    public nav: NavController,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController
  ) {



  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;
      this.getRewards();

    }));

  }



  getRewards() {

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this
      .postCustomerUnassignedRewards(this.id_token)
      .then(
        data =>
        {

          this.loader.dismissAll();
          if( data.points ){

            this.rewards.rewardsModel = data.points;

          }

        }
      );

  }



  postCustomerUnassignedRewards(id_token): Promise<any> {

    let url=  ENV.API_URL + "getUserUnassignedRewards";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);


  }



  openReward(reward){

    this.app.getRootNav().push( CustomerRewardPage, {'reward': reward} );

  }



  handleError() {

    this.loader.dismissAll();

  }

}
