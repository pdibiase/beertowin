import {Component} from "@angular/core";
import {Loading, LoadingController, ModalController, NavController, ToastController} from "ionic-angular";
import {LoginPage} from "../login/login";
import 'rxjs/add/operator/map';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {authProvider} from "../../providers/authProvider";
import {ValidationService} from "../../app/validation.service";
import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import {TranslateService} from "@ngx-translate/core";
import {AvatarService} from "../../services/avatar-service";
import {TermsAndConditionsPage} from "../terms-and-conditions/terms-and-conditions";
import {ENV} from "../../config/environment.prod";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {


  private loader: Loading;
  private error : any;
  private user_nicename : string;
  private user_name : string;
  private user_surname : string;
  private user_gender : string;
  private user_birthday : string;
  private user_email : string;
  private termsAccepted: boolean = false;
  form : FormGroup;


  constructor(
    public modalCtrl: ModalController,
    public formBuilder: FormBuilder,
    public nav: NavController,
    public auth:authProvider,
    public fb: Facebook,
    private translate: TranslateService,
    public avatarService : AvatarService,
    private loading: LoadingController,
    private toastCtrl: ToastController
  ) {

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', [Validators.required, ValidationService.passwordValidator]],
      password_confirmation: ['', [Validators.required, ValidationService.passwordCompareValidator]],
      user_nicename: ['', [Validators.required]],
      user_name: ['', [Validators.required]],
      user_surname: ['', [Validators.required]],
      user_gender: ['m', [Validators.required]],
      user_birthday: ['', [Validators.required]],
      user_telephone1: ['', [Validators.required]],
      user_ZIPcode: ['', [Validators.required]],
      app_code: [ ENV.APP_CODE, [Validators.required]],
      user_privacy_agree: [false, [Validators.required]],
      user_adult: [false, [Validators.required]],
      user_communicate_data_agree: [false],
      user_profiling_agree: [false],
      user_marketing_agree: [false]
    });


    //this.form.get("user_gender").setValue("m");

  }



  // sign up
  doSignup(credentials) {

    if( this.form.valid && this.termsAccepted == true ){

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this.auth.signup(credentials).subscribe(
        data => this.signupSuccess(data),
        err => this.signupError(err),
      );

    }

  }


  signupSuccess(data) {

    this.loader.dismissAll();
    this.error = null;


    if( data ) {

      if(data.result == "true") {

        let toast = this.toastCtrl.create({
          message: this.translate.instant('signup-success'),
          duration: 3000,
          position: 'top'
        });
        toast.present();


        this.nav.setRoot( LoginPage );

      } else {


        let message = (data.countUsers)?
          this.translate.instant('signup-error-user', {
            user_nicename: this.form.controls['user_nicename'].value,
            countUsers: data.countUsers
          }) :
          this.translate.instant('signup-error')

        let toast = this.toastCtrl.create({
          message: message,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      }

    }

  }


  signupError(error){

    this.loader.dismissAll();


    if( error && error.error && error.error.errors ) {

      //SHOW ERROR MESSAGE
      let errorsJson = ['email', 'password', 'user_nicename'];
      errorsJson.forEach( item => {

        let errorItem = error.error.errors[item];
        if( errorItem ) {

          errorItem.forEach( message => {

            let toast = this.toastCtrl.create({
              message: message,
              duration: 3000,
              position: 'top'
            });
            toast.present();

          });

        }

      });

    }

  }


  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }


  doFacebookSignup() {

// Login with permissions
    this.fb.login(['public_profile', 'user_photos', 'email', 'user_birthday'])
      .then( (res: FacebookLoginResponse) => {

        // The connection was successful
        if(res.status == "connected") {

          // Get user ID and Token
          //var fb_id = res.authResponse.userID;
          //var fb_token = res.authResponse.accessToken;

          // Get user infos from the API
          this.fb.api("/me?fields=name,gender,birthday,email,first_name,last_name", []).then((user) => {

            if( user.birthday ) {

              let res = user.birthday.split("/");
              this.user_birthday =  res[1] + "/" + res[0] + "/" + res[2];

            }

            this.user_nicename  = user.name;
            this.user_name      = user.first_name;
            this.user_surname   = user.last_name;
            this.user_gender    = user.gender;
            this.user_email     = user.email;

          });

        }
        // An error occurred while loging-in
        else {

          console.log("An error occurred...");

        }

      })
      .catch((e) => {
        console.log('Error logging into Facebook', e);
      });


    //this.fb.logEvent(this.fb.EVENTS.EVENT_NAME_ADDED_TO_CART);

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(0);

  }


  goToTermsAndConditions() {

    let modal = this.modalCtrl.create(TermsAndConditionsPage);
    modal.onDidDismiss(data => {

      if( data ){

        this.termsAccepted = true;

        this.form.controls['user_privacy_agree'].setValue(data.user_privacy_agree);
        this.form.controls['user_adult'].setValue(data.user_adult);
        this.form.controls['user_communicate_data_agree'].setValue(data.user_communicate_data_agree);
        this.form.controls['user_profiling_agree'].setValue(data.user_profiling_agree);
        this.form.controls['user_marketing_agree'].setValue(data.user_marketing_agree);

      }

    });

    modal.present();

  }

}
