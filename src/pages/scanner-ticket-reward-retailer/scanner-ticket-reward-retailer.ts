import {Component} from "@angular/core";
import {NavParams, ToastController, LoadingController, Loading, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-scanner-ticket-reward-retailer',
  templateUrl: 'scanner-ticket-reward-retailer.html'
})
export class ScannerTicketRewardRetailerPage {

  private id_token;
  public point: any;
  private loader: Loading;


  constructor(
    params: NavParams,
    public nav: NavController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public avatarService : AvatarService
  ) {

    this.point = params.get('point');

  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }


  assignReward(point_id){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    let url=  ENV.API_URL + "assignReward";
    return this
      .http.post(
        url,
        {
          point_id: point_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        this.loader.dismissAll();
        this.nav.pop();

        let toast = this.toastCtrl.create({
          message: this.translate.instant('reward-assigned'),
          duration: 3000,
          position: 'top'
        });
        toast.present();


      })
      .catch(error =>

        this.handleError()

      );

  }


  handleError() {

    this.loader.dismissAll();

  }


}
