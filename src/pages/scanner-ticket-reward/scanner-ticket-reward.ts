import {Component} from "@angular/core";
import {NavController, NavParams, Platform} from "ionic-angular";
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {Vibration} from "@ionic-native/vibration";
import {NativeAudio} from "@ionic-native/native-audio";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-scanner-ticket-reward',
  templateUrl: 'scanner-ticket-reward.html'
})
export class ScannerTicketRewardPage {

  private id_token;
  private avatar_code;
  public reward: any;



  constructor(
    public navCtrl: NavController,
    params: NavParams,
    public http: HttpClient,
    public local: Storage,
    public avatarService : AvatarService,
    private vibration: Vibration,
    public platform: Platform,
    private nativeAudio: NativeAudio
  ) {

    this.reward = params.get('reward');

  }



  ionViewDidLoad() {

    this.reproduceApplause();
    this.makeVibration();

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }


  reproduceApplause() {

    this.nativeAudio.play("applause");

  }


  makeVibration() {

    if( this.platform.is('ios') || this.platform.is('android') ) {

      this.vibration.vibrate([2000,1000,2000]);

    }

  }


  goToScannerPage(){

    this.navCtrl.pop();

  }


}
