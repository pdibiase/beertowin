import {Component} from "@angular/core";
import {NavController, ToastController} from "ionic-angular";
import {AvatarService} from "../../services/avatar-service";
import {Storage} from '@ionic/storage';
import {TranslateService} from "@ngx-translate/core";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-avatars',
  templateUrl: 'avatars.html'
})
export class AvatarsPage {

  public avatars;
  public avatar_code;


  constructor(
    public nav: NavController,
    public avatarService: AvatarService,
    public local: Storage,
    private toastCtrl: ToastController,
    private translate: TranslateService,
  ) {


  }


  ionViewDidLoad() {

    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;
      this.avatars = this.avatarService.getAll();

    }));

  }


  getAvatarClass( avatar_code ) {

    return (this.avatar_code != avatar_code)? 'selected-avatar' : '';

  }


  selectAvatar( avatar_code ) {

    this.avatar_code = avatar_code;
    this.local.set('avatar_code', avatar_code);


    let toast = this.toastCtrl.create({
      message: this.translate.instant('avatar-updated'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }

}
