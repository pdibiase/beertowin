import {Component, QueryList, ViewChildren} from "@angular/core";
import {
  App, ItemSliding, Loading, LoadingController, ModalController, NavController,
  Platform
} from "ionic-angular";
import 'rxjs/add/operator/map';
import {NotificationsModels} from "../../services/notifications";
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {badgeProvider} from "../../providers/badgeProvider";
import {NotificationModalPage} from "../notification-modal/notification-modal";
import {TranslateService} from "@ngx-translate/core";
import {ENV} from "../../config/environment.prod";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {NewsEventDetailPage} from "../news-event-detail/news-event-detail";
import {NewssModels} from "../../services/news";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html'
})
export class NotificationsPage {

  @ViewChildren(ItemSliding) private slidingItems: QueryList<ItemSliding>;

  private id_token;
  private loader: Loading;
  notifications: NotificationsModels = new NotificationsModels();
  news: NewssModels = new NewssModels();
  listType: string = "notifications";




  constructor(
    public app: App,
    public platform: Platform,
    public nav: NavController,
    public modalCtrl: ModalController,
    public http: HttpClient,
    public local: Storage,
    public badgeProvider : badgeProvider,
    private translate: TranslateService,
    private loading: LoadingController,
    private iab: InAppBrowser
  ) {


  }


  ionViewDidLoad() {


  }


  ionViewDidEnter() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;
      this.showNotifications();

    }));

  }


  showNotifications() {

    //this.listType = 'notifications';
    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this.getNotifications()
      .then(
        data => {

          this.loader.dismissAll();
          this.notifications.notificationModel = data;

        }
      )
      .catch( e =>{
        this.handleError();
      });

  }


  showEvents() {


    //this.listType = 'events';
    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this.getEvents()
      .then(
        data => {

          this.loader.dismissAll();

          if( data && data.data ){

            this.news.newsModel = data.data;

          }

        }
      )
      .catch( e =>{
        this.handleError();
      });

  }


  getNotifications(): Promise<any> {

    let url=  ENV.API_URL + "getNotifications";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as NotificationsModels[] )
      .catch(this.handleError);

  }


  getEvents(): Promise<any> {

    let url=  ENV.API_URL + "getNextUserEvents";
    return this
      .http.post(
        url,
        {
          app_code : ENV.APP_CODE
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as NewssModels[] )
      .catch(this.handleError);

  }



  getNotificationClass( is_read ) {

    return (is_read == 0)? 'unread-notification' : '';

  }




  openLink(link){

    if (this.platform.is('android') || this.platform.is('ios')) {


      if( !link.includes("http://") && !link.includes("https://") ) {

        link = "https://" + link;

      }


      this.iab.create(
        link,
        '_system',
        "location=yes"
      );

    } else {

      window.open(link);

    }

  }



  openNotification(notification) {

    if( notification.notification_type == 1 ) { //SIMPLE NOTIFICATION

      this.closeAllItems();
      this.nav.push(
        NotificationModalPage,
        { notification: notification }
      );


    } else if ( notification.notification_type == 2 ) { //NEWS EVENT

      this.getNewsEvent(
        notification.notification_id,
        notification.notification_association_id
      );

    }

  }


  deleteNotification(notification, index){

    let url=  ENV.API_URL + "setNotificationDeleted";
    return this
      .http.post(
        url,
        {
          notification_id : notification.notification_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        let res = JSON.parse(JSON.stringify(response));
        if( res && res.code == 200 ) {

          this.notifications.notificationModel.splice(index, 1);
          this.badgeProvider.decreaseBadges(1);

        }

      })
      .catch(this.handleError);

  }


  getNewsEvent(
    notification_id,
    event_id
  ){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    let url=  ENV.API_URL + "getNewsDetail";
    return this
      .http.post(
        url,
        {
          news_id : event_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        let res = JSON.parse(JSON.stringify(response));
        if( res && res.code == 200 ) {

          this.nav.push(
            NewsEventDetailPage,
            {
              'notification_id' : notification_id,
              'news': res.data
            }
          );

        }

      })
      .catch(this.handleError);

  }


  openEventDetail(news){

    this.app.getRootNav().push( NewsEventDetailPage, {'news': news} );

  }


  handleError() {

    this.loader.dismissAll();

  }


  public openSlidingItem($event: Event, item: any) {
    // This is to prevent a call to itemSliding.close() in the template
    $event.stopPropagation();

    // Close all other open items to have behavior similar to the drag method
    this.closeAllItems();

    // In order for the width of the buttons to be calculated the item
    // must be slightly opened
    item._setOpenAmount(1);

    setTimeout(() => {
      const children = Array.from(
        // use _leftOptions if buttons are on the left (could be made to be dynamic)
        item._rightOptions._elementRef.nativeElement.children,
      );
      // Calculate the width of all of the buttons
      const width = children.reduce(
        (acc: number, child: HTMLElement) => acc + child.offsetWidth,
        0,
      );

      // Open to the calculated width
      item.moveSliding(width);
      item._setOpenAmount(width, false);
    }, 0);
  }


  private closeAllItems() {
    this.slidingItems.map(item => item.close());
  }


  getNotificationImage( image ){

    return (image)? image : "assets/img/logo.png";

  }

}
