import { Component } from '@angular/core';
import {
  AlertController, Loading, LoadingController, ModalController, NavController,
  ToastController
} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {MainTabsPage} from "../main-tabs/main-tabs";
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ScannerAwardPage} from "../scanner-award/scanner-award";
import {ScannerTicketRewardPage} from "../scanner-ticket-reward/scanner-ticket-reward";
import {ScannerThingsPage} from "../scanner-things/scanner-things";


@Component({
  selector: 'page-scanner',
  templateUrl: 'scanner.html'
})
export class ScannerPage {


  private url: any;
  private id_token;
  private qrcode;
  private loader: Loading;
  private showConfetti = false;


  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private barcodeScanner: BarcodeScanner,
    private toastCtrl: ToastController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
  ) {



  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }


  goToHome() {

    this.navCtrl.pop();

  }


  scanBarcode() {

    let options = {
      formats: "QR_CODE",
      showTorchButton: true
    };


    this.barcodeScanner.scan(options).then((barcodeData) => {

      if (barcodeData.cancelled) {

        this.navCtrl.setRoot(MainTabsPage);

      }


      if( barcodeData.text ) {

        let barcodeCode = barcodeData.text.replace(ENV.APP_URL, '');
        this.makeTokenRequest(
          barcodeCode,
          false,
          false,
          false
        );

      }

    }, (err) => {
      console.log(err);
    });


  }



  scanBarcodeTest() {


    let text = "RESET-19.03.3";
    //let text = ENV.APP_URL + "ea757747134a33ff238853008e7d046b"; //PREMIO
    //let text = ENV.APP_URL + "a87ff679a2f3e71d9181a67b7542122c"; //CARD
    //let text = ENV.APP_URL + "984211987f91f2caacdc338ac1c70c24"; //TICKET


    let barcodeCode = text.replace(ENV.APP_URL, '');
    this.makeTokenRequest(
      barcodeCode,
      false,
      false,
      false
    );

  }



  makeTokenRequest(
    barcodeCode,
    agree,
    thing_vote,
    thing_id
  ){

    this
      .sendToken(
        barcodeCode,
        agree,
        thing_vote,
        thing_id
      )
      .then(
        data => {

          this.loader.dismissAll();


          //IF CODE == 200 SHOW SUCCESS TOAST
          //IF CODE == 201 SHOW SUCCESS CARD REQUIRE
          //IF CODE == 202 END CARD, SHOW POPUP REQUIRE AWARD
          //IF CODE == 203 TICKET WINNER
          //IF CODE == 204 TICKET NOT WINNER
          let message = '';
          if( data && data.code ) {

            if( data.code == 200 ) {

              message = this.translate.instant('send-qrcode-success');

            } else if( data.code == 201 ) {

              message = this.translate.instant('send-qrcode-success-request');

            } else if ( data.code == 202 && data.card_id ) {

              message = this.translate.instant('send-qrcode-success');
              this.navCtrl.push(ScannerAwardPage, { card_id: data.card_id });

            } else if ( data.code == 203 ) { //TICKET WINNER

              this.showConfetti = true;
              this.navCtrl.push(ScannerTicketRewardPage, {reward: data.reward});


            } else if( data.code == 204 ) { //TICKET NOT WINNER

              message = this.translate.instant('send-qrcode-success-no-win-request');

            } else if( data.code == 205 && data.things ) {

              message = this.translate.instant('send-qrcode-select-things');
              this.openModalThings( data.things );

            }

          }


          let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
          });
          toast.present();

        }
      );

  }



  sendToken(
    qrcode,
    agree,
    thing_vote,
    thing_id
  ): Promise<any> {

    this.qrcode = qrcode;
    this.url=  ENV.API_URL + "scanTicket";


    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let body = {};
    body['qrcode'] = qrcode;
    body['app_code'] = ENV.APP_CODE;
    if( agree ){ body['agree'] = agree; }
    if( thing_vote ){ body['campaignuserthinsvote_vote'] = thing_vote; }
    if( thing_id ){ body['campaignuserthinsvote_thing_id'] = thing_id; }


    return this
      .http.post(
        this.url,
        body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch( error => this.handleError(error));

  }


  handleError(error) {

    if( error && error.error && error.error.message ) {


      //SHOW ERROR MESSAGE
      error.error.message.forEach( item => {

        let toast = this.toastCtrl.create({
          message: item,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });


      //IF ERROR IS 401 OPEN POPUP
      //CUSTOMER ISN'T ON RETAILER LIST
      if( error.error.code && error.error.code == 401 ) {

        this.openPopupConfirmations();

      } else if( error.error.code && error.error.code == 407 ) {

        this.openPopupWrongApp( error.error.message[0]);

      }

    }

  }


  openPopupConfirmations() {

    let alert = this.alertCtrl.create({
      title: this.translate.instant('confirm-connection-title'),
      message: this.translate.instant('confirm-connection-text'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => { }
        },
        {
          text: this.translate.instant('ok'),
          handler: () => {

            this.makeTokenRequest(
              this.qrcode,
              true,
              false,
              false
            );

            /*this.sendToken(
              this.qrcode,
              true
            ).then(
              data => {

                this.loader.dismissAll();

                //IF CODE == 200 SHOW SUCCESS TOAST
                if( data && data.code && data.code == 200 ) {

                  let toast = this.toastCtrl.create({
                    message: this.translate.instant('send-qrcode-success'),
                    duration: 3000,
                    position: 'top'
                  });
                  toast.present();

                }

              }
            );*/

          }
        }
      ]
    });

    alert.present();

  }


  openModalThings( things ){

    let thingModal = this.modalCtrl.create(ScannerThingsPage, { things: things });
    thingModal.onDidDismiss(data => {

      this.makeTokenRequest(
        this.qrcode,
        false,
        data.thing_vote,
        data.thing_id
      );

    });
    thingModal.present();

  }


  openPopupWrongApp( url ) {

    let alert = this.alertCtrl.create({
      title: this.translate.instant('wrong-app-title'),
      message: this.translate.instant('wrong-app-text'),
      buttons: [
        {
          text: this.translate.instant('cancel'),
          role: 'cancel',
          handler: () => { }
        },
        {
          text: this.translate.instant('install'),
          handler: () => {

            this.iab.create(
              url,
              '_system',
              "location=yes"
            );

          }
        }
      ]
    });

    alert.present();

  }

}
