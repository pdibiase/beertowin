import {Component} from "@angular/core";
import {Loading, LoadingController, NavController} from "ionic-angular";
import {LandingModel, LandingModels} from "../../services/landings";
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {TranslateService} from "@ngx-translate/core";
import {authProvider} from "../../providers/authProvider";
import {RetailerDetailPage} from "../retailer-detail/retailer-detail";
import {ENV} from "../../config/environment.prod";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-retailers-my-list',
  templateUrl: 'retailers-my-list.html'
})
export class RetailersMyListPage {


  private id_token;
  private loader: Loading;
  retailers: LandingModels = new LandingModels();
  simpleSearchTerms: string = '';


  constructor(
    public auth: authProvider,
    public nav: NavController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
  ) {


  }

  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;
      this.simpleSearchRetailers( true );

    }));

  }



  simpleSearchRetailers( forceSearch ) {

    if( this.simpleSearchTerms.length > 2 || forceSearch ) {

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this
        .simpleSearch()
        .then(
          data => {

            this.loader.dismissAll();
            this.retailers.landingModel = data;

          }
        );

    }

  }



  simpleSearch(): Promise<any> {

    let url= ENV.API_URL + "getAssignedRetailers";
    return this
      .http.post(
        url,
        {
          'search'        : this.simpleSearchTerms,
          'app_code'      : ENV.APP_CODE
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as LandingModel[] )
      .catch(this.handleError);

  }


  handleError() {

    this.loader.dismissAll();

  }


  getLandingImage( retailer ){


    let image = "assets/img/pub_default.jpg";

    let galleryArr = retailer.user_landing_gallery;
    if( galleryArr && galleryArr.length > 0 ) {

      image = galleryArr[Math.floor(Math.random() * galleryArr.length)];

    }

    return image;

  }



  printAttributeArray( ecard, attribute ) {

    let attributes = ecard;
    return (attributes && attributes[attribute])? attributes[attribute]: "";

  }


  openDetails (retailer) {

    this.nav.push(RetailerDetailPage, { retailer: retailer });

  }


  getRetailerLogo( logo ){

    return (logo)? logo : "assets/img/logo.png";

  }


}
