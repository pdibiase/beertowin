import {Component} from "@angular/core";
import {Loading, LoadingController, NavController} from "ionic-angular";
import {MyProfilePage} from "../my-profile/my-profile";
import {MyProfileUpdatePasswordPage} from "../my-profile-update-password/my-profile-update-password";
import {MyProfileUpdateAccountPage} from "../my-profile-update-account/my-profile-update-account";
import {ENV} from "../../config/environment.prod";
import {Storage} from '@ionic/storage';
import {TranslateService} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html'
})
export class SettingPage {


  private id_token;
  private loader: Loading;


  constructor(
    public nav: NavController,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    public http: HttpClient
  ) {



  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }


  openAccountPage(){

    this.nav.push(MyProfilePage);

  }


  openUpdateAccountPage() {

    this.nav.push(MyProfileUpdateAccountPage);

  }


  openUpdatePasswordPage(){

    this.nav.push(MyProfileUpdatePasswordPage);

  }


  doResetAccount(){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this
      .postResetAccount()
      .then(
        data => {

          this.loader.dismissAll();

        }
      );

  }


  postResetAccount(): Promise<any> {

    let url=  ENV.API_URL + "resetUser";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


  handleError() {

    this.loader.dismissAll();

  }


}
