import {Component} from "@angular/core";
import {Events, NavController, NavParams, ToastController, ViewController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import {ThingsModels} from "../../services/things";
import {TranslateService} from "@ngx-translate/core";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-scanner-things',
  templateUrl: 'scanner-things.html'
})
export class ScannerThingsPage {

  private id_token;
  public thingVote = -1;
  public thingSelected = -1;
  public things = new ThingsModels();



  @ViewChild(Slides) slides: Slides;
  constructor(
    params: NavParams,
    public nav: NavController,
    public local: Storage,
    public events: Events,
    private toastCtrl: ToastController,
    private translate: TranslateService,
    private viewCtrl: ViewController
  ) {

    this.things.thingModel = params.get('things');


    events.subscribe(
      'star-rating:changed',
      (starRating) =>
      {

        this.thingVote = starRating;

      }
    );

  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }


  slideChanged() {

    this.thingSelected = this.slides.getActiveIndex();
    this.thingVote = -1;

  }


  setVote(thingVote){

    this.thingVote = thingVote;

  }


  saveVote(i) {

    let thing = this.things.thingModel[i];

    //CHECK IF USER HAVE VOTED
    if( this.thingVote > -1 ) {

      this.viewCtrl.dismiss(
        {
          'thing_id': thing.thing_id,
          'thing_vote': this.thingVote
        }
      );

    } else {

      let toast = this.toastCtrl.create({
        message: this.translate.instant('error-vote-thing'),
        duration: 3000,
        position: 'top'
      });
      toast.present();

    }


  }


}
