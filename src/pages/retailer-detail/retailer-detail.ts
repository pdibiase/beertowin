import {Component, ViewChild} from "@angular/core";
import {Content, Loading, LoadingController, NavController, NavParams, Platform, ToastController} from "ionic-angular";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {SocialSharing} from "@ionic-native/social-sharing";
import {TranslateService} from "@ngx-translate/core";
import {Storage} from '@ionic/storage';
import {HttpClient} from "@angular/common/http";
import {ENV} from "../../config/environment.prod";
import {CardsModels} from "../../services/cards";
import {CardDetailPage} from "../card-detail/card-detail";
import {RetailerGalleryPage} from "../retailer-gallery/retailer-gallery";
import {CardsPage} from "../cards/cards";
import {Camera} from '@ionic-native/camera';
import {FileTransfer} from "@ionic-native/file-transfer";
import {CampaignsModels} from "../../services/campaigns";


declare var google: any;

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-retailer-detail',
  templateUrl: 'retailer-detail.html'
})
export class RetailerDetailPage {
  @ViewChild(Content) content: Content;


  private id_token;
  public retailer;
  public galleryArr;
  public selfieArr;
  public hoursArr;
  public ecardArr;
  public tagsArr;
  public map: any;
  public landingImage;
  public totalResults: number = 0;
  public serviceTabColor: string = '#ffb336';
  public galleryTabColor: string = '#4C4D4B';
  public aboutTabColor: string = '#4C4D4B';
  public cardsTabColor: string = '#4C4D4B';
  private loader: Loading;
  cards: CardsModels = new CardsModels();
  campaignsInstantWin: CampaignsModels = new CampaignsModels();



  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public platform: Platform,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing,
    private translate: TranslateService,
    public local: Storage,
    private toastCtrl: ToastController,
    private loading: LoadingController,
    public http: HttpClient,
    public camera: Camera,
    public transfer: FileTransfer
  ) {

    this.retailer = navParams.get('retailer');

    this.galleryArr = this.retailer.user_landing_gallery;
    this.selfieArr = [];
    this.hoursArr = this.retailer.user_landing_schedule;
    this.ecardArr = this.retailer.user_ecard;
    this.tagsArr = (this.retailer.user_landing_services)? this.retailer.user_landing_services.split(","): [];

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      //SET LANGING IMAGE
      this.setLandingImage();

      // INIT MAP
      //this.initializeMap();


      //GET CARDS
      this
        .getActiveCards(this.retailer.user_id)
        .then(
          data => {

            this.updateResultRequests();
            this.cards.cardModel = data;

          }
        );


      this
        .getRetailerSelfies(this.retailer.user_id)
        .then(
          data => {

            this.updateResultRequests();

            if( data && data.data ){

              this.selfieArr = data.data;

            }

          }
        );


      this
        .getRetailerIntantWinCampaigns(this.retailer.user_id)
        .then(
          data => {

            this.updateResultRequests();

            if( data && data.data ){

              this.campaignsInstantWin.campaignModel = data.data;

            }

          }
        );


      //ADD LANDING TOTAL COUNT
      this
        .addCountUserInfo(this.retailer.id);

    }));

  }



  updateResultRequests(){

    this.totalResults++;
    if( this.totalResults == 2 ){

      this.loader.dismissAll();

    }

  }


  getRetailerIntantWinCampaigns(id): Promise<any> {

    let url=  ENV.API_URL + "getRetailerCampaignInstantWin";
    return this
      .http.post(
        url,
        {
          retailer_id: id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


  getRetailerSelfies(id): Promise<any> {

    let url=  ENV.API_URL + "getRetailerSelfies";
    return this
      .http.post(
        url,
        {
          retailer_id: id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


  getActiveCards(id): Promise<any> {

    let url=  ENV.API_URL + "getCustomerCards";
    return this
      .http.post(
        url,
        {
          card_retailer_id: id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as CardsModels[] )
      .catch(this.handleError);

  }


  addCountUserInfo(id): Promise<any> {

    let url=  ENV.API_URL + "addUserInfoCount";
    return this
      .http.post(
        url,
        {
          id : id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


  handleError() {

    this.updateResultRequests();

  }


  initializeMap() {

    if( this.ecardArr && this.ecardArr.user_geoLat && this.ecardArr.user_geoLon ) {

      let latLng = new google.maps.LatLng(
        this.ecardArr.user_geoLat,
        this.ecardArr.user_geoLon
      );


      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false
      };


      this.map = new google.maps.Map(document.getElementById("map-detail"), mapOptions);
      new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: this.map.getCenter()
      });


      // refresh map
      setTimeout(() => {
        google.maps.event.trigger(this.map, 'resize');
      }, 300);

    }


  }



  setLandingImage(){

    this.landingImage = "assets/img/pub_default.jpg";
    if( this.galleryArr && this.galleryArr.length > 0 ) {

      let randomIndex = Math.floor(Math.random() * this.galleryArr.length);
      this.landingImage = this.galleryArr[randomIndex];

    }

  }



  printAttributeArray( attribute ) {

    return (this.ecardArr && this.ecardArr[attribute])? this.ecardArr[attribute]: "";

  }


  openLink( link ){

    if( link ){

      if( !link.includes("http://") && !link.includes("https://") ) {

        link = "https://" + link;

      }

      this.iab.create(
        link,
        '_system',
        "location=yes"
      );

    }

  }



  openSocialSharing( name, url ) {

    if( url ){

      this.sendShare(name, name, url);

    } else {

      this.local.get('user_name').then((user_name => {

        this.local.get('user_surname').then((user_surname => {

          let username =  user_name + " " + user_surname;
          this.sendShare(
            this.translate.instant('user-are-on-pub', {username: username, retailer: name}),
            name,
            null
          );

        }));

      }));

    }



  }


  sendShare(message, subject, url) {

    this.socialSharing.share(message, subject, "", url)
      .then(() => {

        let toast = this.toastCtrl.create({
          message: this.translate.instant('share-success'),
          duration: 3000,
          position: 'top'
        });
        toast.present();

      }).catch(() => {

      let toast = this.toastCtrl.create({
        message: this.translate.instant('share-error'),
        duration: 3000,
        position: 'top'
      });
      toast.present();

    });

  }


  printSheduleDay( schedule ) {

    return (schedule)? schedule : this.translate.instant('closed');

  }


  openCardDetails(card) {

    this.nav.push(CardDetailPage, { card: card });

  }


  scrollTo(element: string) {

    this.serviceTabColor = (element == 'services')? '#ffb336' : '#4C4D4B';
    this.galleryTabColor = (element == 'gallery')? '#ffb336' : '#4C4D4B';
    this.aboutTabColor = (element == 'about')? '#ffb336' : '#4C4D4B';


    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 1000)

  }


  openRetailerGallery(item){

    this.nav.push(
      RetailerGalleryPage,
      {
        'galleryArr': this.galleryArr,
        'showSlide': item
      });

  }


  openRetailerSelfies(item){

    this.nav.push(
      RetailerGalleryPage,
      {
        'galleryArr': this.selfieArr,
        'showSlide': item
      });

  }


  makeCall(phone){

    if(phone){

      document.location.href = "tel:" + phone;

    }

  }


  openWhatsappConversation(phone){

    if(phone){

      document.location.href = "https://api.whatsapp.com/send?phone=" + phone;

    }

  }


  sendEmail(email){

    if(email){

      document.location.href = "mailto:" + email;

    }

  }


  openLogoFullPage(logo) {

    this.nav.push(
      RetailerGalleryPage,
      {
        'galleryArr': [logo],
        'showSlide': logo
      });

  }


  openCards(){

    this.nav.push(CardsPage, {
      cards : this.cards,
      landing : 'landing'
    });

  }


  makeSelfie(){

    let options = {
      quality: 80,
      targetWidth: 1000,
      targetHeight: 1000,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.CAMERA
    };

    this.camera.getPicture(options).then((imageData) => {


      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();


      this
        .sendSelfie('data:image/jpeg;base64,'+imageData)
        .then(
          data => {

            this.loader.dismissAll();
            let toast = this.toastCtrl.create({
              message: (data.code == 200)? this.translate.instant('selfie-success') : this.translate.instant('selfie-error'),
              duration: 3000,
              position: 'top'
            });
            toast.present();

          }
        );

    });

  }



  sendSelfie(image): Promise<any> {

    let url=  ENV.API_URL + "sendSelfie/" + this.retailer.id;
    return this.http.post(
      url,
      {
        file: image,
        landing_id: this.retailer.id
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept':"application/json",
          'Authorization': "Bearer " + this.id_token
        }
      }
    )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


}
