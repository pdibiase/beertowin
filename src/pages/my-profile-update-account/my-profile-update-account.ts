import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, ToastController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {authProvider} from "../../providers/authProvider";
import {Facebook} from '@ionic-native/facebook';
import {TranslateService} from "@ngx-translate/core";
import {ENV} from "../../config/environment.prod";
import {Storage} from '@ionic/storage';
import {HttpClient} from "@angular/common/http";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-my-profile-update-account',
  templateUrl: 'my-profile-update-account.html'
})
export class MyProfileUpdateAccountPage {


  private loader: Loading;
  private error : any;
  form : FormGroup;
  private id_token;
  public user;


  constructor(
    public formBuilder: FormBuilder,
    public nav: NavController,
    public auth:authProvider,
    public fb: Facebook,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public local: Storage,
    public http: HttpClient
  ) {

    this.form = this.formBuilder.group({
      user_name: ['', [Validators.required]],
      user_surname: ['', [Validators.required]],
      user_gender: ['', [Validators.required]],
      user_birthday: ['', [Validators.required]],
      user_telephone1: ['', [Validators.required]],
      user_ZIPcode: ['', [Validators.required]]
    });

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this
        .getCustomer()
        .then(
          data => {

            this.loader.dismissAll();
            this.user = data.user;


            this.form.controls['user_name'].setValue(this.user.user_name);
            this.form.controls['user_surname'].setValue(this.user.user_surname);
            this.form.controls['user_gender'].setValue(this.user.user_gender);
            this.form.controls['user_birthday'].setValue(this.user.user_birthday);
            this.form.controls['user_telephone1'].setValue(this.user.user_telephone1);
            this.form.controls['user_ZIPcode'].setValue(this.user.user_ZIPcode);

          }
        );

    }));

  }


  getCustomer(): Promise<any> {

    let url= ENV.API_URL + "getUser";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }


  handleError() {

    if( this.loader ) {

      this.loader.dismissAll();

    }

  }


  // sign up
  doUpdateAccount(credentials) {

    if( this.form.valid ){

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this.auth.updateAccount(credentials, this.id_token).subscribe(
        data => this.updateAccountSuccess(data),
        err => this.updateAccountError(err),
      );

    }

  }


  updateAccountSuccess(data) {

    this.loader.dismissAll();
    this.error = null;


    this.local.set('user_name',  this.form.value['user_name']);
    this.local.set('user_surname', this.form.value['user_surname']);


    let toast = this.toastCtrl.create({
      message: this.translate.instant('update-account-success'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }


  updateAccountError(err){

    this.loader.dismissAll();

    let toast = this.toastCtrl.create({
      message: this.translate.instant('error-update-account'),
      duration: 3000,
      position: 'top'
    });
    toast.present();

  }

}
