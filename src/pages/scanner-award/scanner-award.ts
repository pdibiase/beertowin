import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {ENV} from "../../config/environment.prod";
import {MainTabsPage} from "../main-tabs/main-tabs";
import {TranslateService} from "@ngx-translate/core";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-scanner-award',
  templateUrl: 'scanner-award.html'
})
export class ScannerAwardPage {

  private id_token;
  private avatar_code;
  public card_id: any;
  private loader: Loading;


  constructor(
    params: NavParams,
    public nav: NavController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public avatarService : AvatarService
  ) {

    this.card_id = params.get('card_id');

  }



  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }


  getAward(){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    let url=  ENV.API_URL + "requestCampaignAward";
    return this
      .http.post(
        url,
        {
          card_id: this.card_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        this.loader.dismissAll();


        let toast = this.toastCtrl.create({
          message: this.translate.instant('award-required'),
          duration: 3000,
          position: 'top'
        });
        toast.present();


        this.nav.setRoot( MainTabsPage );

      })
      .catch(error =>

        this.handleError(error)

      );

  }


  goBack() {

    this.nav.pop();

  }


  handleError(error) {

    this.loader.dismissAll();

    if( error && error.error && error.error.message ) {


      //SHOW ERROR MESSAGE
      error.error.message.forEach( item => {

        let toast = this.toastCtrl.create({
          message: item,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });

    }

  }


}
