import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, Platform} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {AvatarService} from "../../services/avatar-service";
import {NewsModel} from "../../services/news";
import {ENV} from "../../config/environment.prod";
import {HttpClient} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";
import {InAppBrowser} from "@ionic-native/in-app-browser";


@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html'
})
export class NewsDetailPage {

  private id_token;
  private avatar_code;
  newsLike : number = -1;
  private loader: Loading;
  news: NewsModel = new NewsModel();


  constructor(
    public nav: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public avatarService : AvatarService,
    public local: Storage,
    public http: HttpClient,
    private translate: TranslateService,
    private loading: LoadingController,
    private iab: InAppBrowser
  ) {

    this.news = navParams.get('news');

  }

  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this
        .getLikeNews(this.news.news_id)
        .then(
          data => {

            this.loader.dismissAll();
            if(data && data.data && data.data != null){

              this.newsLike = data.data;

            }

          }
        );


    }));


    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }



  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }


  setVote(news_id, like){

    this
      .sendNewsVote(news_id, like)
      .then(
        data => {

          this.loader.dismissAll();
          if( data && data.code == 200 ) {

            this.newsLike = like;

          }

        });

  }


  getLikeNews(news_id): Promise<any> {

    let url=  ENV.API_URL + "getNewsVote";
    return this
      .http.post(
        url,
        {
          'news_id' : news_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as JSON )
      .catch(this.handleError);

  }



  sendNewsVote(news_id, like): Promise<any> {

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let url=  ENV.API_URL + "setNewsVote";
    return this
      .http.post(
        url,
        {
          'news_id' : news_id,
          'like'    : like
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch(this.handleError);

  }



  handleError() {

    if( this.loader ) {

      this.loader.dismissAll();

    }

  }


  openLink(link){

    if (this.platform.is('android') || this.platform.is('ios')) {


      if( !link.includes("http://") && !link.includes("https://") ) {

        link = "https://" + link;

      }


      this.iab.create(
        link,
        '_system',
        "location=yes"
      );

    } else {

      window.open(link);

    }

  }


}
