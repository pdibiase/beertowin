import {Component} from "@angular/core";
import {Loading, LoadingController, NavController} from "ionic-angular";
import {ENV} from "../../config/environment.prod";
import {HttpClient} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";
import {Storage} from '@ionic/storage';
import {UserModel} from "../../services/user";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html'
})
export class MyProfilePage {


  private id_token;
  private loader: Loading;
  public user_name;
  public user_surname;
  public user: UserModel = new UserModel();


  constructor(
    public nav: NavController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    ) {


  }


  ionViewDidLoad() {

    this.getUserName();


    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this
        .getCustomer()
        .then(
          data => {

            this.loader.dismissAll();
            this.user = data.user;

          }
        );

    }));

  }


  getUserName() {

    this.local.get('user_name').then((user_name => {

      this.user_name = user_name;

    }));


    this.local.get('user_surname').then((user_surname => {

      this.user_surname = user_surname;

    }));

  }


  getCustomer(): Promise<any> {

    let url= ENV.API_URL + "getUser";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response as UserModel )
      .catch(this.handleError);

  }


  handleError() {

    if( this.loader ) {

      this.loader.dismissAll();

    }

  }


}
