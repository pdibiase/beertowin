import {Component} from "@angular/core";
import {Loading, LoadingController, NavController, NavParams, ToastController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {AssignableRewardsModels} from "../../services/assignable-rewards";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-assign-rewards',
  templateUrl: 'assign-rewards.html'
})
export class AssignRewardsPage {


  private id_token;
  private loader: Loading;
  rewards: AssignableRewardsModels = new AssignableRewardsModels();



  constructor(
    public http: HttpClient,
    public nav: NavController,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    private toastCtrl: ToastController,
    public navParams: NavParams
  ) {


  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;
      this.getAssignableRewards();

    }));

  }



  getAssignableRewards() {

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this
      .postAssignableRewards(this.id_token)
      .then(
        data =>
        {

          this.loader.dismissAll();
          if( data.points ){

            this.rewards.assignableRewardsModel = data.points;

          }

        }
      );

  }



  postAssignableRewards(id_token): Promise<any> {

    let url=  ENV.API_URL + "getAssignableRewards";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);


  }


  assignReward(point_id){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    let url=  ENV.API_URL + "assignReward";
    return this
      .http.post(
        url,
        {
          point_id: point_id
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        this.loader.dismissAll();
        this.nav.pop();

        let toast = this.toastCtrl.create({
          message: this.translate.instant('reward-assigned'),
          duration: 3000,
          position: 'top'
        });
        toast.present();


      })
      .catch(error =>

        this.handleError()

      );

  }



  handleError() {

    this.loader.dismissAll();

  }

}
