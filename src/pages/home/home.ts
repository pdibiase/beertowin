import {Component} from "@angular/core";
import {App, Events, Loading, LoadingController, NavController} from "ionic-angular";
import {HotelService} from "../../services/hotel-service";
import {RestaurantService} from "../../services/restaurant-service";
import {AttractionService} from "../../services/attraction-service";
import {ScannerPage} from "../scanner/scanner";
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {NotificationsPage} from "../notifications/notifications";
import {badgeProvider} from "../../providers/badgeProvider";
import {ENV} from "../../config/environment.prod";
import {NewssModels} from "../../services/news";
import {TranslateService} from "@ngx-translate/core";
import {NewsDetailPage} from "../news-detail/news-detail";
import {NewsPollDetailPage} from "../news-poll-detail/news-poll-detail";
import {NewsEventDetailPage} from "../news-event-detail/news-event-detail";
import {AvatarService} from "../../services/avatar-service";

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private url: any;
  private id_token;
  public restaurants: any;
  public hotels: any;
  public attractions: any;
  public unreadNotifications: number;
  newsws: NewssModels = new NewssModels();
  private loader: Loading;
  private avatar_code;


  constructor(
    public app: App,
    public nav: NavController,
    public hotelService: HotelService,
    public restaurantService: RestaurantService,
    public attractionService: AttractionService,
    public http: HttpClient,
    public local: Storage,
    public badgeProvider : badgeProvider,
    private translate: TranslateService,
    private loading: LoadingController,
    public events: Events,
    public avatarService : AvatarService,
  ) {

    // set sample data
    this.restaurants = restaurantService.getAll();
    this.hotels = hotelService.getAll();
    this.attractions = attractionService.getAll();


    events.subscribe('home:updateNews', () => {

      this.ionViewDidEnter();

    });

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      this.storeAppToken(id_token);

    }));

    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));

  }


  ionViewDidEnter() {

    this.local.get('avatar_code').then((avatar_code => {

      this.avatar_code = avatar_code;

    }));


    this.local.get('id_token').then((id_token => {

      this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
      this.loader.present();

      this
        .getLastNews(id_token)
        .then(
          data => {

            this.loader.dismissAll();

            if (data && data.data) {

              this.newsws.newsModel = data.data;

            }

          }
        );

    }));

  }


  storeAppToken(id_token) {

    this.local.get('app_token').then((app_token => {


      if( app_token ) {

        //this.local.get('stored_app_token').then((stored_app_token => {

          //if( !stored_app_token || app_token != stored_app_token ) {

            let url =  ENV.API_URL + "addDeviceToken";


            this.http.post(
              url,
              {
                token : app_token,
                app_code : ENV.APP_CODE
              },
              {
                headers: {
                  'Content-Type': 'application/json',
                  'Accept':"application/json",
                  'Authorization': "Bearer " + this.id_token
                }
              })
              .toPromise()
              .then(data => {

                  if ( data ){

                    this.local.set('stored_app_token', app_token);

                  }

                },
                error  => {

                  //TODO

                })
              .catch(this.handleError);


          //}

        //}));


      }


    }));

  }


  getLastNews(id_token): Promise<any> {

    let url=  ENV.API_URL + "getLastNews?app_code=" + ENV.APP_CODE;
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }




  countUnreadNotifications() {

    this.url=  ENV.API_URL + "countUnreadNotifications";
    return this
      .http.get(
        this.url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        if( response ) {

          let res = JSON.parse(JSON.stringify(response));
          this.badgeProvider.setBadges( parseInt(res) );


          //SET NOTIFICATIONS FOR INTERNAL BADGE
          this.local.set('unread_notifications', parseInt(res));
          this.unreadNotifications = parseInt(res);

        }

      })
      .catch(this.handleError);

  }


  handleError() {

    if( this.loader ) {

      this.loader.dismissAll();

    }

  }


  openNotificationsPage() {

    this.nav.push( NotificationsPage );

  }


  openNews(news){

    if( news.news_category == 1 ) { //EVENT

      this.app.getRootNav().push( NewsEventDetailPage, {'news': news} );

    } else if( news.news_category == 2 ) { //NEWS

      this.app.getRootNav().push( NewsDetailPage, {'news': news} );

    } else { //POLL

      this.app.getRootNav().push( NewsPollDetailPage, {'news': news} );

    }

  }


  openScan(){
    this.app.getRootNav().push(ScannerPage);
  }



  getAvatarImage() {

    return this.avatarService.getItemImage(this.avatar_code);

  }

}
