import {Component} from "@angular/core";
import {Events, NavController, Platform} from "ionic-angular";
import {HomePage} from "../home/home";
import {ScannerPage} from "../scanner/scanner";
import {RetailersListPage} from "../retailers-list/retailers-list";
import {RetailersMyListPage} from "../retailers-my-list/retailers-my-list";
import {Storage} from "@ionic/storage";
import {NotificationsPage} from "../notifications/notifications";
import {badgeProvider} from "../../providers/badgeProvider";
import {HttpClient} from "@angular/common/http";
import {ENV} from "../../config/environment.prod";


/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-main-tabs',
  templateUrl: 'main-tabs.html'
})
export class MainTabsPage {
  // set tabs
  public tabHome = HomePage;
  public unreadNotifications: number;
  private url: any;
  public id_token;


  constructor(
    public platform: Platform,
    public nav: NavController,
    public local: Storage,
    public http: HttpClient,
    public badgeProvider : badgeProvider,
    public events: Events
  ) {



  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

      //UPDATE BADGE NUMBER
      this.countUnreadNotifications();

      //REQUEST BADGE PERMISSION
      this.badgeProvider.requestPermission();

    }));

  }


  ionViewDidEnter() {

    //UPDATE BADGE NUMBER
    if( this.id_token ){

      this.countUnreadNotifications();
      this.events.publish('home:updateNews');

    };

  }


  countUnreadNotifications() {

    this.url=  ENV.API_URL + "countUnreadNotifications";
    return this
      .http.get(
        this.url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => {

        if( response ) {

          let res = JSON.parse(JSON.stringify(response));
          this.badgeProvider.setBadges( parseInt(res) );


          //SET NOTIFICATIONS FOR INTERNAL BADGE
          this.local.set('unread_notifications', parseInt(res));
          this.unreadNotifications = parseInt(res);

        }

      })
      .catch(this.handleError);

  }


  handleError() {

  }


  openScan() {

    this.nav.push(ScannerPage);

  }


  openMyRetailers() {

    this.nav.push(RetailersMyListPage);

  }


  openSearch() {

    this.nav.push(RetailersListPage);

  }



  openCoupons() {

    //TODO

  }


  openNotificationsPage(){

    this.nav.push(NotificationsPage);

  }


}
