import { Component } from '@angular/core';
import { AlertController, Loading, LoadingController, ModalController, NavController, ToastController, MenuController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {HttpClient} from "@angular/common/http";
import {Storage} from '@ionic/storage';
import {MainTabsPage} from "../main-tabs/main-tabs";
import {ENV} from "../../config/environment.prod";
import {TranslateService} from "@ngx-translate/core";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ScannerAwardPage} from "../scanner-award/scanner-award";
import {ScannerTicketRewardPage} from "../scanner-ticket-reward/scanner-ticket-reward";
import {ScannerTicketRewardRetailerPage} from "../scanner-ticket-reward-retailer/scanner-ticket-reward-retailer";
import {AssignRewardsPage} from "../assign-rewards/assign-rewards";
import {LoginPage} from "../login/login";
import {authProvider} from "../../providers/authProvider";


@Component({
  selector: 'page-scanner',
  templateUrl: 'home-retailer.html'
})
export class HomeRetailerPage {


  private url: any;
  private id_token;
  private qrcode;
  private loader: Loading;
  private showConfetti = false;


  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private barcodeScanner: BarcodeScanner,
    private toastCtrl: ToastController,
    public http: HttpClient,
    public local: Storage,
    private translate: TranslateService,
    private loading: LoadingController,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
    private menu: MenuController,
    public authProvider: authProvider
  ) {

    this.menu.swipeEnable(false, 'menu-left');

  }


  ionViewDidEnter() {

    this.menu.swipeEnable(false);

  }

  ionViewWillLeave() {

    this.menu.swipeEnable(true);

  }


  ionViewDidLoad() {

    this.local.get('id_token').then((id_token => {

      this.id_token = id_token;

    }));

  }


  openAssignRewardsIntantWinPage() {

    this.navCtrl.push(AssignRewardsPage, {
      cards: false,
      instantwin: true
    });

  }


  scanBarcodeAssignCardReward() {

    let options = {
      formats: "QR_CODE",
      showTorchButton: true
    };


    this.barcodeScanner.scan(options).then((barcodeData) => {

      if (barcodeData.cancelled) {

        this.navCtrl.setRoot(MainTabsPage);

      }


      if( barcodeData.text ) {

        let barcodeCode = barcodeData.text;

        this
          .sendScanBarcodeAssignCardReward( barcodeCode )
          .then(
            data => {

              this.loader.dismissAll();


              //IF CODE == 200 SHOW SUCCESS POPUP
              //IF CODE == 201 NO TIKET
              let message = '';
              if( data && data.code ) {

                if( data.code == 200 ) {

                  message = this.translate.instant('assign-card-reward-success');

                } else {

                  message = this.translate.instant('assign-card-reward-error');

                }

              }


              let toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'top'
              });
              toast.present();

            }
          );

      }

    }, (err) => {
      console.log(err);
    });


  }


  scanBarcode() {

    let options = {
      formats: "QR_CODE",
      showTorchButton: true
    };


    this.barcodeScanner.scan(options).then((barcodeData) => {

      if (barcodeData.cancelled) {

        this.navCtrl.setRoot(MainTabsPage);

      }


      if( barcodeData.text ) {

        let barcodeCode = barcodeData.text.replace(ENV.APP_URL, '');

        this
          .sendToken( barcodeCode, false )
          .then(
            data => {

              this.loader.dismissAll();


              //IF CODE == 200 SHOW SUCCESS POPUP
              //IF CODE == 201 NO TIKET
              let message = '';
              if( data && data.code ) {

                if( data.code == 200 ) {

                  message = this.translate.instant('check-ticket-success');
                  this.navCtrl.push(ScannerTicketRewardRetailerPage, { point: data.point });

                } else if( data.code == 201 ) {

                  message = this.translate.instant('no-tiket-used-request');

                }

              }


              let toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'top'
              });
              toast.present();

            }
          );

      }

    }, (err) => {
      console.log(err);
    });


  }


  scanBarcodePackage() {

    let options = {
      formats: "QR_CODE",
      showTorchButton: true
    };


    this.barcodeScanner.scan(options).then((barcodeData) => {

      if (barcodeData.cancelled) {

        this.navCtrl.setRoot(MainTabsPage);

      }


      if( barcodeData.text ) {

        let barcodeCode = barcodeData.text.replace(ENV.APP_URL, '');

        this
          .assignPackageSendToken( barcodeCode, false )
          .then(
            data => {

              this.loader.dismissAll();


              //IF CODE == 200 SHOW SUCCESS
              let message = '';
              if( data && data.code && data.code == 200 ) {

                message = this.translate.instant('package-assigned-success');

              } else {

                message = this.translate.instant('package-assigned-error');

              }


              let toast = this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'top'
              });
              toast.present();

            }
          );

      }

    }, (err) => {
      console.log(err);
    });


  }



  sendScanBarcodeAssignCardReward(qrcode): Promise<any> {

    this.qrcode = qrcode;
    this.url=  ENV.API_URL + "assignCustomerCardReward";


    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let body = {};
    body['card_id'] = qrcode;
    body['app_code'] = ENV.APP_CODE;


    return this
      .http.post(
        this.url,
        body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch( error => this.handleError(error));

  }



  sendToken(qrcode, agree): Promise<any> {

    this.qrcode = qrcode;
    this.url=  ENV.API_URL + "checkTicket";


    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let body = {};
    body['qrcode'] = qrcode;
    body['app_code'] = ENV.APP_CODE;
    if( agree ){ body['agree'] = agree; }


    return this
      .http.post(
        this.url,
        body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch( error => this.handleError(error));

  }


  assignPackageSendToken(qrcode, agree): Promise<any> {

    this.qrcode = qrcode;
    this.url=  ENV.API_URL + "assignPackageToRetailer";


    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();


    let body = {};
    body['qrcode'] = qrcode;
    body['app_code'] = ENV.APP_CODE;
    if( agree ){ body['agree'] = agree; }


    return this
      .http.post(
        this.url,
        body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch( error => this.handleError(error));

  }


  handleError(error) {

    if( this.loader ) {

      this.loader.dismissAll();

    }


    if( error && error.error && error.error.message ) {


      //SHOW ERROR MESSAGE
      error.error.message.forEach( item => {

        let toast = this.toastCtrl.create({
          message: item,
          duration: 3000,
          position: 'top'
        });
        toast.present();

      });

    }

  }


  doLogout() {

    let alert = this.alertCtrl.create({
      title: this.translate.instant('logout-title'),
      subTitle: this.translate.instant('logout-text'),
      buttons: [
        {
          text: this.translate.instant('dismiss'),
          role: 'cancel'
        },
        {
          text: 'LOGOUT',
          handler: () => {

            this.authProvider.logout();
            this.navCtrl.setRoot(LoginPage);

          }
        }
      ]
    });
    alert.present();

  }




  doResetAccount(){

    this.loader = this.loading.create({content: this.translate.instant('loading-popup')});
    this.loader.present();

    this
      .postResetAccount()
      .then(
        data => {

          this.loader.dismissAll();

        }
      );

  }


  postResetAccount(): Promise<any> {

    let url=  ENV.API_URL + "resetRetailerUser";
    return this
      .http.get(
        url,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + this.id_token
          }
        }
      )
      .toPromise()
      .then(response => response )
      .catch(this.handleError);

  }

}
