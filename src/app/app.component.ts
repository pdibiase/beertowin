import {Component} from '@angular/core';
import {AlertController, Platform} from 'ionic-angular';
import {ViewChild} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

// import pages
import {WelcomePage} from '../pages/welcome/welcome';
import {LoginPage} from '../pages/login/login';
import {MainTabsPage} from '../pages/main-tabs/main-tabs';
import {SettingPage} from '../pages/setting/setting';
import {Storage} from '@ionic/storage';
import {ScannerPage} from "../pages/scanner/scanner";
import { TranslateService } from '@ngx-translate/core';
import {NotificationsPage} from "../pages/notifications/notifications";
import {FCM} from "@ionic-native/fcm";
import {badgeProvider} from "../providers/badgeProvider";
import {AvatarsPage} from "../pages/avatars/avatars";
import {CardsPage} from "../pages/cards/cards";
import {RetailersListPage} from "../pages/retailers-list/retailers-list";
import {AuthService} from "../services/AuthService";
import {ENV} from "../config/environment.prod";
import {HomeRetailerPage} from "../pages/home-retailer/home-retailer";
import {authProvider} from "../providers/authProvider";
import {CustomerRewardsPage} from "../pages/customer-rewards/customer-rewards";
import {NativeAudio} from "@ionic-native/native-audio";
import {WelcomeMenuPage} from "../pages/welcome-menu/welcome-menu";
import {InAppBrowser} from "@ionic-native/in-app-browser";
// end import pages

@Component({
  templateUrl: 'app.html',
  queries: {
    nav: new ViewChild('content')
  }
})
export class MyApp {

  public rootPage: any;
  public username: string;
  public nav: any;
  public auth: any;

  public pages = [
    {
      title: 'Home',
      count: 0,
      component: MainTabsPage,
      icon: 'ios-home-outline'
    },

    {
      title: 'Tutorial',
      count: 0,
      component: WelcomeMenuPage,
      icon: 'ios-information-circle-outline'
    },

    {
      title: 'Avatar',
      count: 0,
      component: AvatarsPage,
      icon: 'ios-contacts-outline'
    },

    {
      title: 'Cards',
      count: 0,
      component: CardsPage,
      icon: 'ios-photos-outline'
    },

    {
      title: 'Pub',
      count: 0,
      component: RetailersListPage,
      icon: 'ios-pint-outline'
    },

    {
      title: 'Rewards',
      count: 0,
      component: CustomerRewardsPage,
      icon: 'ribbon'
    },

    {
      title: 'Scanner',
      count: 0,
      component: ScannerPage,
      icon: 'ios-qr-scanner-outline'
    },

    {
      title: 'Settings',
      count: 0,
      component: SettingPage,
      icon: 'ios-settings-outline'
    },

    {
      title: 'Termini e Condizioni',
      count: 0,
      component: SettingPage,
      icon: 'ios-information-circle-outline'
    },


    {
      title: 'Logout',
      count: 0,
      component: LoginPage,
      icon: 'ios-log-out'
    },

  ];


  constructor(
    public platform: Platform,
    statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public local: Storage,
    public translate: TranslateService,
    public fcm: FCM,
    public badgeProvider : badgeProvider,
    private alertCtrl: AlertController,
    public authProvider: authProvider,
    private nativeAudio: NativeAudio,
    private iab: InAppBrowser
  ) {

    this.auth = AuthService;
    this.setUsername();
    this.local.get('id_token').then((id_token => {


      if(id_token != null) {

        if ( this.auth.authenticated('id_token')) {

          this.local.get('user_type').then((user_type => {

            if( user_type && user_type == ENV.TYPE_RETAILER ) {

              this.rootPage = HomeRetailerPage;

            } else {

              this.rootPage = MainTabsPage;

            }

          }));

        } else{

          this.rootPage = LoginPage;

        }

      } else {

        this.rootPage = WelcomePage;

      }

    }));


    translate.setDefaultLang('it');
    platform.ready().then(() => {


      //Preloading Audio
      this.nativeAudio.preloadComplex('applause', 'assets/audio/applause.wav', 1, 1, 0);


      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.hide();
      splashScreen.hide();




      //SET NOTIFICATIONS TOKENS
      if( platform.is('ios') || platform.is('android') ) {


        this.fcm.subscribeToTopic('all');
        this.fcm.getToken().then(token => {

          this.local.set('app_token', token);

        });



        this.fcm.onNotification().subscribe(data => {

          //INCREASE BADGE
          badgeProvider.increaseBadges(1);


          if(data.wasTapped) {

            this.local.get('id_token').then((token) => {

              if( token != null ) {

                this.nav.push(NotificationsPage, { profileId: data.profileId });

              } else {

                this.nav.setRoot(LoginPage, { profileId: data.profileId });

              }

            });

          }

        });


        this.fcm.onTokenRefresh().subscribe(token => {

          // backend.registerToken(token);
          this.local.set('app_token', token);

        });


      }


    });

  }


  setUsername() {

    this.local.get('user_name').then((user_name => {

      this.local.get('user_surname').then((user_surname => {

        this.username =  user_name + " " + user_surname;

      }));

    }));

  }


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    if(page.title == 'Logout') {

      this.doLogout();

    } else if(page.title == 'Home') {

      this.nav.setRoot(page.component);

    } else if( page.title == 'Termini e Condizioni' ) {

      this.iab.create(
         ENV.TERMS_PDF,
        '_system',
        "location=yes"
      );

    } else {

      this.nav.push(page.component);

    }

  }



  doLogout() {

    let alert = this.alertCtrl.create({
      title: this.translate.instant('logout-title'),
      subTitle: this.translate.instant('logout-text'),
      buttons: [
        {
          text: this.translate.instant('dismiss'),
          role: 'cancel'
        },
        {
          text: 'LOGOUT',
          handler: () => {

            this.authProvider.logout();

            this.rootPage = LoginPage;
            this.nav.setRoot(this.rootPage);

          }
        }
      ]
    });
    alert.present();

  }

}
