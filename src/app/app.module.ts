import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import { QRCodeModule } from 'angular2-qrcode';

// import services
import {FoodCategoryService} from '../services/food-category-service';
import {FoodService} from '../services/food-service';
import {GatewayService} from '../services/gateway-service';
import {HotelService} from '../services/hotel-service';
import {RestaurantService} from '../services/restaurant-service';
import {ActivityService} from '../services/activity-service';
import {AttractionService} from '../services/attraction-service';
import {ContactService} from '../services/contact-service';
// end import services

// import pages
import {ActivityPage} from '../pages/activity/activity';
import {AttractionDetailPage} from '../pages/attraction-detail/attraction-detail';
import {AttractionsPage} from '../pages/attractions/attractions';
import {FindFriendPage} from '../pages/find-friend/find-friend';
import {HomePage} from '../pages/home/home';
import {HotelDetailPage} from '../pages/hotel-detail/hotel-detail';
import {HotelsPage} from '../pages/hotels/hotels';
import {LoginPage} from '../pages/login/login';
import {MainTabsPage} from '../pages/main-tabs/main-tabs';
import {ModalFilterPage} from '../pages/modal-filter/modal-filter';
import {MyProfilePage} from '../pages/my-profile/my-profile';
import {RegisterPage} from '../pages/register/register';
import {RestaurantDetailPage} from '../pages/restaurant-detail/restaurant-detail';
import {RestaurantsPage} from '../pages/restaurants/restaurants';
import {SearchPage} from '../pages/search/search';
import {SettingPage} from '../pages/setting/setting';
import {WelcomePage} from '../pages/welcome/welcome';
import {ScannerPage} from "../pages/scanner/scanner";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import { HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera';
import { FileTransfer} from '@ionic-native/file-transfer';
// end import pages

//JWT Config
import {authProvider} from '../providers/authProvider';
import { AuthHttp, AuthConfig} from 'angular2-jwt';
import {HttpClient} from "@angular/common/http";
import {IonicStorageModule} from "@ionic/storage";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {NotificationsPage} from "../pages/notifications/notifications";
import {RetailersListPage} from "../pages/retailers-list/retailers-list";
import {RetailersMyListPage} from "../pages/retailers-my-list/retailers-my-list";
import { FCM } from '@ionic-native/fcm';
import {ControlMessages} from "../components/control-messages/control-messages";
import {Facebook} from "@ionic-native/facebook";
import {Badge} from "@ionic-native/badge";
import {badgeProvider} from "../providers/badgeProvider";
import {NotificationModalPage} from "../pages/notification-modal/notification-modal";
import {AvatarsPage} from "../pages/avatars/avatars";
import {AvatarService} from "../services/avatar-service";
import {TermsAndConditionsPage} from "../pages/terms-and-conditions/terms-and-conditions";
import {RetailerDetailPage} from "../pages/retailer-detail/retailer-detail";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {SocialSharing} from "@ionic-native/social-sharing";
import {MyProfileUpdatePasswordPage} from "../pages/my-profile-update-password/my-profile-update-password";
import {MyProfileUpdateAccountPage} from "../pages/my-profile-update-account/my-profile-update-account";
import {LostPasswordPage} from "../pages/lost-password/lost-password";
import {CardsPage} from "../pages/cards/cards";
import {CardDetailPage} from "../pages/card-detail/card-detail";
import {ScannerAwardPage} from "../pages/scanner-award/scanner-award";
import {RetailerGalleryPage} from "../pages/retailer-gallery/retailer-gallery";
import {NewsDetailPage} from "../pages/news-detail/news-detail";
import {NewsEventDetailPage} from "../pages/news-event-detail/news-event-detail";
import {NewsPollDetailPage} from "../pages/news-poll-detail/news-poll-detail";
import {AuthService} from "../services/AuthService";
import {ScannerTicketRewardPage} from "../pages/scanner-ticket-reward/scanner-ticket-reward";
import {HomeRetailerPage} from "../pages/home-retailer/home-retailer";
import {ScannerTicketRewardRetailerPage} from "../pages/scanner-ticket-reward-retailer/scanner-ticket-reward-retailer";
import {AssignRewardsPage} from "../pages/assign-rewards/assign-rewards";
import {CustomerRewardsPage} from "../pages/customer-rewards/customer-rewards";
import {CustomerRewardPage} from "../pages/customer-reward/customer-reward";
import {Vibration} from "@ionic-native/vibration";
import {NativeAudio} from "@ionic-native/native-audio";
import {ScannerThingsPage} from "../pages/scanner-things/scanner-things";
import {StarRatingModule} from "ionic3-star-rating";
import {CardDetailModalPage} from "../pages/card-detail-modal/card-detail-modal";
import {WelcomeMenuPage} from "../pages/welcome-menu/welcome-menu";


export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true
  }), http);
}



@NgModule({
  declarations: [
    MyApp,
    ActivityPage,
    AssignRewardsPage,
    AttractionDetailPage,
    AttractionsPage,
    AvatarsPage,
    CardDetailPage,
    CardDetailModalPage,
    CardsPage,
    CustomerRewardPage,
    CustomerRewardsPage,
    FindFriendPage,
    HomePage,
    HomeRetailerPage,
    HotelDetailPage,
    HotelsPage,
    LoginPage,
    LostPasswordPage,
    NewsDetailPage,
    NewsEventDetailPage,
    NewsPollDetailPage,
    NotificationsPage,
    NotificationModalPage,
    MainTabsPage,
    ModalFilterPage,
    MyProfilePage,
    MyProfileUpdateAccountPage,
    MyProfileUpdatePasswordPage,
    RegisterPage,
    RestaurantDetailPage,
    RestaurantsPage,
    RetailerDetailPage,
    RetailerGalleryPage,
    RetailersListPage,
    RetailersMyListPage,
    ScannerPage,
    ScannerTicketRewardPage,
    ScannerTicketRewardRetailerPage,
    ScannerAwardPage,
    ScannerThingsPage,
    SearchPage,
    SettingPage,
    TermsAndConditionsPage,
    WelcomePage,
    WelcomeMenuPage,
    ControlMessages
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    QRCodeModule,
    StarRatingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (setTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AssignRewardsPage,
    AvatarsPage,
    CardDetailPage,
    CardDetailModalPage,
    CardsPage,
    CustomerRewardPage,
    CustomerRewardsPage,
    HomePage,
    HomeRetailerPage,
    LoginPage,
    LostPasswordPage,
    NewsDetailPage,
    NewsEventDetailPage,
    NewsPollDetailPage,
    NotificationsPage,
    NotificationModalPage,
    MainTabsPage,
    MyProfilePage,
    MyProfileUpdateAccountPage,
    MyProfileUpdatePasswordPage,
    RegisterPage,
    RetailerDetailPage,
    RetailerGalleryPage,
    RetailersListPage,
    RetailersMyListPage,
    ScannerPage,
    ScannerTicketRewardPage,
    ScannerTicketRewardRetailerPage,
    ScannerAwardPage,
    ScannerThingsPage,
    SettingPage,
    TermsAndConditionsPage,
    WelcomePage,
    WelcomeMenuPage
  ],
  providers: [
    AuthService,
    AvatarService,
    Badge,
    badgeProvider,
    BarcodeScanner,
    Camera,
    StatusBar,
    SplashScreen,
    Facebook,
    FCM,
    FileTransfer,
    FoodCategoryService,
    FoodService,
    GatewayService,
    HotelService,
    InAppBrowser,
    NativeAudio,
    SocialSharing,
    RestaurantService,
    Vibration,
    ActivityService,
    AttractionService,
    /* import services */
    ContactService,
    authProvider,
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps:[HttpClient]
    },
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    }
  ]

})


export class AppModule {

}


export function setTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
