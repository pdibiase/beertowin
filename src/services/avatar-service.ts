import {Injectable} from "@angular/core";
import {Storage} from '@ionic/storage';
import {AVATARS} from "./mock-avatars";


@Injectable()
export class AvatarService {
  private avatars:any;

  constructor(
    public local: Storage
  ) {
    this.avatars = AVATARS;
  }


  getAll() {
    return this.avatars;
  }


  getItem(id) {

    for (var i = 0; i < this.avatars.length; i++) {
      if (this.avatars[i].id === parseInt(id)) {
        return this.avatars[i];
      }
    }

    return null;

  }


  getItemImage(avatar_code){

    let image = "assets/img/avatars/avW1.png";
    let avatar = this.getItem(avatar_code);
    if( avatar != null ) {

      image = avatar.face;

    }

    return image;

  }


  remove(item) {
    this.avatars.splice(this.avatars.indexOf(item), 1);
  }
}
