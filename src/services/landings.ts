export class LandingModels {
  landingModel: Array<LandingModel>
}

export class LandingModel {
  user_nicename: string;
  user_ecard: LandingEcardModel;
}


export class LandingEcardModel{
  user_address: string;
  user_city: string;
  user_phone: string;
}
