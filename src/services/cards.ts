export class CardsModels {
  cardModel: Array<CardModel>
}


export class CardModel {
  member_points: number;
  member_total_points: number;
  campaign_points: number;
}
