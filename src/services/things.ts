export class ThingsModels {
  thingModel: Array<ThingModel>
}


export class ThingModel {
  thing_id: number;
  thing_title: string;
  thing_description: string;
  thing_image: string;
  thing_vote_type: number;
}
