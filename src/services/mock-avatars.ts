export let AVATARS = [
  {
    id: 1,
    name: 'Rosy',
    subtitle: 'La rossa',
    lastText: '<br />"Sono forte e focosa...<br /> ma amo la birra ghiacciata!"',
    face: 'assets/img/avatars/avW1.png'
  },
  {
    id: 2,
    name: 'Nerdy',
    subtitle: 'Lo smart',
    lastText: '<br />"Amici, algoritmi e birra,<br /> la mia serata perfetta!"',
    face: 'assets/img/avatars/avM1.png'
  },
  {

    id: 3,
    name: 'Mary',
    subtitle: 'La sognatrice',
    lastText: '<br />"Cenetta romantica<br /> con due calici di birra?"',
    face: 'assets/img/avatars/avW2.png'
  },
  {
    id: 4,
    name: 'George',
    subtitle: 'Il bello',
    lastText: '<br />"Ciao baby,<br /> che ne dici di una buona birra?"',
    face: 'assets/img/avatars/avM2.png'
  },
  {
    id: 5,
    name: 'Blondy',
    subtitle: 'La fashion blogger',
    lastText: '"Dimmi che birra bevi...<br /> e ti dirò chi sei!"',
    face: 'assets/img/avatars/avW3.png'
  },
  {
    id: 6,
    name: 'Ferdy',
    subtitle: 'No limits',
    lastText: '<br />"Birra, hamburger e patatine,<br /> da domani sono a dieta!"',
    face: 'assets/img/avatars/avM3.png'
  }
];
