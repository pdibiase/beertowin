export class UserModel {
  email: string;
  user_nicename: string;
  user_birthday: string;
  user_telephone1: string;
  user_ZIPcode: string;
}
