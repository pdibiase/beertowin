export class NotificationsModels {
  notificationModel: Array<NotificationModel>
}


export class NotificationModel {
  notification_type: number;
  notification_association_id: number;
  notification_title: string;
  notification_body: string;
}
