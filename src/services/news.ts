export class NewssModels {
  newsModel: Array<NewsModel>
}


export class NewsModel {
  news_id: number;
  news_app_id: number;
  news_title: string;
  news_description: string;
  news_link: string;
  news_category: string;
  news_date_start_visibility: string;
  news_date_end_visibility: string;
  news_affermative_button: string;
  news_negative_button: string;
  news_date_event: string;
  news_wepay_view: number;
  news_wepay_interact: number;
  news_wepay_share: number;
  news_total_likes: number;
  news_total_unlikes: number;
  news_image: string;
}
