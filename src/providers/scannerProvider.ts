import { Injectable } from '@angular/core';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class scanProvider {

  private url: any;


  constructor(
    private barcodeScanner: BarcodeScanner,
    public myHttp: HttpClient) {



  }


  makeScan(id_token){

    let options = {
      formats: "QR_CODE",
      showTorchButton: true
    };


    this.barcodeScanner.scan(options).then((barcodeData) => {

      //TODO
      /*if (barcodeData.cancelled) {

        this.navCtrl.setRoot(MainTabsPage);

      }*/


      if( barcodeData.text ) {

        this.sendToken(
          barcodeData.text,
          id_token
        );

      }

    }, (err) => {
      console.log(err);
    });

  }


  sendToken(qrcode, id_token): Promise<any> {

    qrcode = "c4ca4238a0b923820dcc509a6f75849b";
    this.url= "https://demoxtw.effetreweb2.net/public/api/scanTicket";


    return this
      .myHttp.post(
        this.url,
        {
          qrcode: qrcode,
          agree: true,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept':"application/json",
            'Authorization': "Bearer " + id_token
          }
        }
      )
      .toPromise()
      .then(response => response)
      .catch(this.handleError);

  }



  handleError() {

    //TODO

  }

}
