import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";
import {HttpClient} from "@angular/common/http";
import {badgeProvider} from "./badgeProvider";
import {ENV} from "../config/environment.prod";

@Injectable()
export class authProvider {

  LOGIN_URL: string = ENV.API_URL + "login";
  SIGNUP_URL: string = ENV.API_URL + "signup";
  UPDATE_ACCOUNT_URL: string = ENV.API_URL + "updateUser";
  UPDATE_PASSWORD_URL: string = ENV.API_URL + "updatePassword";
  LOST_PASSWORD_URL: string = ENV.API_URL + "forgot/password";


  // When the page loads, we want the Login segment to be selected
  authType: string = "login";
  // We need to set the content type for the server
  jwtHelper: JwtHelper = new JwtHelper();
  local:Storage;
  user:string;
  http:HttpClient;

  constructor(
    public myStorage: Storage,
    public myHttp: HttpClient,
    public badgeProvider: badgeProvider) {

    this.local = myStorage;
    this.http = myHttp;
    this.local.get('profile').then(profile => {
      this.user = JSON.parse(profile);
    }).catch(error => {
      console.log(error);
    });

  }


  login(credentials) {

    return this.http.post(
      this.LOGIN_URL,
      credentials,
      {
        headers:{
          'Accept':"application/json"
        }
      }
    )
      .map(res => res )

  }


  signup(credentials) {

    return this.http.post(
      this.SIGNUP_URL,
      credentials,
      {
        headers: {
          'Accept':"application/json"
        }
      }
     )
      .map(res => res)

  }


  updateAccount(credentials, id_token){

    return this.http.post(
      this.UPDATE_ACCOUNT_URL,
      credentials,
      {
        headers: {
          'Accept':"application/json",
          'Authorization': "Bearer " + id_token
        }
      }
    )
      .map(res => res);

  }


  updatePassword(credentials, id_token){

    return this.http.post(
      this.UPDATE_PASSWORD_URL,
      credentials,
      {
        headers: {
          'Accept':"application/json",
          'Authorization': "Bearer " + id_token
        }
      }
    )
      .map(res => res);

  }



  lostPassword(credentials) {

    return this.http.post(
      this.LOST_PASSWORD_URL,
      credentials,
      {
        headers: {
          'Accept':"application/json"
        }
      }
    )
      .map(res => res)

  }



  logout() {

    this.local.remove('id_token');
    this.badgeProvider.clearBadges();
    this.user = null;

  }

}
