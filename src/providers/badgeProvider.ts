import { Injectable } from '@angular/core';
import {Badge} from "@ionic-native/badge";

@Injectable()
export class badgeProvider {


  constructor(
    public badge: Badge) {


  }


  async requestPermission() {

    try {
      let hasPermission = await this.badge.hasPermission();
      console.log(hasPermission);
      if (!hasPermission) {
        let permission = await this.badge.requestPermission();
        console.log(permission);
      }
    } catch (e) {
      console.error(e);
    }

  }



  async setBadges(badgeNumber: number) {

    try {
      let badges = await this.badge.set(badgeNumber);
      console.log(badges);
    } catch (e) {
      console.error(e);
    }

  }



  async getBadges(badgeAmount) {

    try {
      badgeAmount = await this.badge.get();
      console.log(badgeAmount);
    }
    catch (e) {
      console.error("APP: " + e);
    }

  }


  async increaseBadges(badgeNumber: number) {

    try {
      let badge = await this.badge.increase(Number(badgeNumber));
      console.log(badge);
    } catch (e) {
      console.error(e);
    }

  }


  async decreaseBadges(badgeNumber: number) {

    try {
      let badge = await this.badge.decrease(Number(badgeNumber));
      console.log(badge);
    } catch (e) {
      console.error(e);
    }

  }


  async clearBadges(){

    try {
      let badge = await this.badge.clear();
      console.log(badge);
    }
    catch(e){
      console.error(e);
    }

  }


}
