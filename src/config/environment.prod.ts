export const ENV = {
  PRODUCTION      : true,
  API_URL         : 'https://demoxtw.effetreweb2.net/public/api/',
  APP_URL         : 'https://beertowin.it/btw/',
  TERMS_PDF       : "https://demoxtw.effetreweb2.net/public/pdf/termini-e-condizioni.pdf",
  APP_CODE        : 'BTW',
  TYPE_RETAILER   : 6
};
